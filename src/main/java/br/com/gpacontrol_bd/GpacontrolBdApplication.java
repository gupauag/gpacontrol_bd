package br.com.gpacontrol_bd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GpacontrolBdApplication {

	public static void main(String[] args) {
		SpringApplication.run(GpacontrolBdApplication.class, args);
	}

}
