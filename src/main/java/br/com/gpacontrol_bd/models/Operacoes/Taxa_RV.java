package br.com.gpacontrol_bd.models.Operacoes;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Taxa_RV {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private double taxLiquidacao;
    private double taxEmonumento;
    private double taxCorretagem;
    private double taxAdministrativa;

    public Taxa_RV() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getTaxLiquidacao() {
        return taxLiquidacao;
    }

    public void setTaxLiquidacao(double taxLiquidacao) {
        this.taxLiquidacao = taxLiquidacao;
    }

    public double getTaxEmonumento() {
        return taxEmonumento;
    }

    public void setTaxEmonumento(double taxEmonumento) {
        this.taxEmonumento = taxEmonumento;
    }

    public double getTaxCorretagem() {
        return taxCorretagem;
    }

    public void setTaxCorretagem(double taxCorretagem) {
        this.taxCorretagem = taxCorretagem;
    }

    public double getTaxAdministrativa() {
        return taxAdministrativa;
    }

    public void setTaxAdministrativa(double taxAdministrativa) {
        this.taxAdministrativa = taxAdministrativa;
    }
}
