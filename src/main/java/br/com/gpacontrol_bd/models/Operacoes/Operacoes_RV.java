package br.com.gpacontrol_bd.models.Operacoes;

import br.com.gpacontrol_bd.models.Ativos.Ativo_RV;
import br.com.gpacontrol_bd.models.Usuario;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Operacoes_RV {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private Date dtCompra;

    private long qntCompra;

    private double vlrCompra;

    @ManyToOne
    private Ativo_RV ativo_rv;

    @ManyToOne
    private Usuario usuario;

    @OneToOne
    private Taxa_RV taxa_rv;

    public Operacoes_RV() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDtCompra() {
        return dtCompra;
    }

    public void setDtCompra(Date dtCompra) {
        this.dtCompra = dtCompra;
    }

    public long getQntCompra() {
        return qntCompra;
    }

    public void setQntCompra(long qntCompra) {
        this.qntCompra = qntCompra;
    }

    public double getVlrCompra() {
        return vlrCompra;
    }

    public void setVlrCompra(double vlrCompra) {
        this.vlrCompra = vlrCompra;
    }

    public Ativo_RV getAtivo_rv() {
        return ativo_rv;
    }

    public void setAtivo_rv(Ativo_RV ativo_rv) {
        this.ativo_rv = ativo_rv;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Taxa_RV getTaxa_rv() {
        return taxa_rv;
    }

    public void setTaxa_rv(Taxa_RV taxa_rv) {
        this.taxa_rv = taxa_rv;
    }
}
