package br.com.gpacontrol_bd.models.Tipos;

import br.com.gpacontrol_bd.models.Tipos.Enums.TipoBanlanceEnum;
import br.com.gpacontrol_bd.models.Usuario;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

public class TipoBalance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private TipoBanlanceEnum tipoBalance;

    @OneToOne
    private Usuario usuario;

    public TipoBalance() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public TipoBanlanceEnum getTipoBalance() {
        return tipoBalance;
    }

    public void setTipoBalance(TipoBanlanceEnum tipoBalance) {
        this.tipoBalance = tipoBalance;
    }
}
