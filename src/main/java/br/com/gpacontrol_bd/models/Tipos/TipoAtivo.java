package br.com.gpacontrol_bd.models.Tipos;

import br.com.gpacontrol_bd.models.Tipos.Enums.ClassificacaoTipoAtivoEnum;
import br.com.gpacontrol_bd.models.Tipos.Enums.TipoAtivoEnum;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class TipoAtivo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private TipoAtivoEnum tipoAtivo;

    private ClassificacaoTipoAtivoEnum classificacaoAtivo;

    public TipoAtivo() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TipoAtivoEnum getTipoAtivo() {
        return tipoAtivo;
    }

    public void setTipoAtivo(TipoAtivoEnum tipoAtivo) {
        this.tipoAtivo = tipoAtivo;
    }

    public ClassificacaoTipoAtivoEnum getClassificacaoAtivo() {
        return classificacaoAtivo;
    }

    public void setClassificacaoAtivo(ClassificacaoTipoAtivoEnum classificacaoAtivo) {
        this.classificacaoAtivo = classificacaoAtivo;
    }
}
