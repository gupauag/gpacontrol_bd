package br.com.gpacontrol_bd.models.Tipos.Enums;

public enum TipoAtivoEnum {
    A("FII"),
    B("Ações"),
    C("ETF"),
    D("Tesouro Direto"),
    E("Fundos"),
    F("DBR"),
    G("Ações Extrangeiras");

    public String valorAtivo;
    TipoAtivoEnum(String valor) {
        valorAtivo = valor;
    }
    public String getValor(){
        return valorAtivo;
    }
}
