package br.com.gpacontrol_bd.models.Tipos.Enums;

public enum ClassificacaoTipoAtivoEnum {
    A("Renda Variável"),
    B("Renda Fixa");

    public String valorClassificacao;
    ClassificacaoTipoAtivoEnum(String valor) {
        valorClassificacao = valor;
    }
    public String getValorClassificacao(){
        return valorClassificacao;
    }
}
