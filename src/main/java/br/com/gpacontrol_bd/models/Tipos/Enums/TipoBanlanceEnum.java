package br.com.gpacontrol_bd.models.Tipos.Enums;

public enum TipoBanlanceEnum {
    A("Peso"),
    B("Porcentagem");

    public String valorBalance;
    TipoBanlanceEnum(String valor) {
        valorBalance = valor;
    }
    public String getValorBalance(){
        return valorBalance;
    }
}
