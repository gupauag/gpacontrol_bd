package br.com.gpacontrol_bd.models.Carteira;

import br.com.gpacontrol_bd.models.Ativos.Ativo_RV;
import br.com.gpacontrol_bd.models.Tipos.TipoAtivo;
import br.com.gpacontrol_bd.models.Usuario;

import javax.persistence.*;

@Entity
public class Carteira_RV {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private double precoTeto;

    private double peso;

    @OneToOne
    private Ativo_RV ativo_rv;

    @ManyToOne
    private TipoAtivo tipoAtivo;

    @ManyToOne
    private Usuario usuario;

    public Carteira_RV() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getPrecoTeto() {
        return precoTeto;
    }

    public void setPrecoTeto(double precoTeto) {
        this.precoTeto = precoTeto;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public Ativo_RV getAtivo_rv() {
        return ativo_rv;
    }

    public void setAtivo_rv(Ativo_RV ativo_rv) {
        this.ativo_rv = ativo_rv;
    }

    public TipoAtivo getTipoAtivo() {
        return tipoAtivo;
    }

    public void setTipoAtivo(TipoAtivo tipoAtivo) {
        this.tipoAtivo = tipoAtivo;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
