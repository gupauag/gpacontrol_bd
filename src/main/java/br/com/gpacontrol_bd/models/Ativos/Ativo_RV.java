package br.com.gpacontrol_bd.models.Ativos;

import br.com.gpacontrol_bd.models.Tipos.TipoAtivo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class Ativo_RV {

    @Id
    private String codAtivo;

    private String nomeEmpresa;
    private String nomeAtivo;
    private String cnpj;
    private Date dtAtualizacao;

    @ManyToOne
    private TipoAtivo tipoAtivo;

    @ManyToOne
    private Segmento_RV segmentoRV;

    public Ativo_RV() {
    }

    public Date getDtAtualizacao() {
        return dtAtualizacao;
    }

    public void setDtAtualizacao(Date dtAtualizacao) {
        this.dtAtualizacao = dtAtualizacao;
    }

    public String getCodAtivo() {
        return codAtivo;
    }

    public void setCodAtivo(String codAtivo) {
        this.codAtivo = codAtivo;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public TipoAtivo getTipoAtivo() {
        return tipoAtivo;
    }

    public void setTipoAtivo(TipoAtivo tipoAtivo) {
        this.tipoAtivo = tipoAtivo;
    }

    public Segmento_RV getSegmentoRV() {
        return segmentoRV;
    }

    public void setSegmentoRV(Segmento_RV segmentoRV) {
        this.segmentoRV = segmentoRV;
    }

    public String getNomeAtivo() {
        return nomeAtivo;
    }

    public void setNomeAtivo(String nomeAtivo) {
        this.nomeAtivo = nomeAtivo;
    }
}
