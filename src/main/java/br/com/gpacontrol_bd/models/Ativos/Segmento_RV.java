package br.com.gpacontrol_bd.models.Ativos;

import br.com.gpacontrol_bd.models.Ativos.Enums.SegmentoRVEnum;
import br.com.gpacontrol_bd.models.Tipos.TipoAtivo;

import javax.persistence.*;

@Entity
public class Segmento_RV {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    //classifica se é bancario, seguros, tijolo, CRI,...
    private SegmentoRVEnum nomeSegmento;

    @ManyToOne
    private TipoAtivo tipoAtivo;

    public Segmento_RV() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SegmentoRVEnum getNomeSegmento() {
        return nomeSegmento;
    }

    public void setNomeSegmento(SegmentoRVEnum nomeSegmento) {
        this.nomeSegmento = nomeSegmento;
    }

    public TipoAtivo getTipoAtivo() {
        return tipoAtivo;
    }

    public void setTipoAtivo(TipoAtivo tipoAtivo) {
        this.tipoAtivo = tipoAtivo;
    }
}
